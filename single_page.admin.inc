<?php

/**
 * @file
 * Admin Settings form for the Single Page Website module.
 */
function single_page_settings_form($form, &$form_state) {
  $form['load_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Load mode'),
    '#required' => TRUE,
    '#default_value' => variable_get('single_page_load_mode', 'all'),
    '#options' => array('all' => 'All pages', 'one' => 'Only active page'),
    '#description' => "Menu items can either be loaded all at once (faster navigation), or on demand (faster page load)",
  );
  $path = libraries_get_path('jquery.easing') . '/jquery.easing.js';
  if (file_exists($path)) {
    $form['easing'] = array(
      '#type' => 'select',
      '#title' => t('Easing'),
      '#required' => FALSE,
      '#default_value' => variable_get('single_page_easing', 'jswing'), // added default value.
      '#options' => array("jswing" => "jswing", "def" => "def",
        "easeInQuad" => "easeInQuad",
        "easeOutQuad" => "easeOutQuad",
        "easeInOutQuad" => "easeInOutQuad",
        "easeInCubic" => "easeInCubic",
        "easeOutCubic" => "easeOutCubic",
        "easeInOutCubic" => "easeInOutCubic",
        "easeInQuart" => "easeInQuart",
        "easeOutQuart" => "easeOutQuart",
        "easeInOutQuart" => "easeInOutQuart",
        "easeInQuint" => "easeInQuint",
        "easeOutQuint" => "easeOutQuint",
        "easeInOutQuint" => "easeInOutQuint",
        "easeInSine" => "easeInSine",
        "easeOutSine" => "easeOutSine",
        "easeInOutSine" => "easeInOutSine",
        "easeInExpo" => "easeInExpo",
        "easeOutExpo" => "easeOutExpo",
        "easeInOutExpo" => "easeInOutExpo",
        "easeInCirc" => "easeInCirc",
        "easeOutCirc" => "easeOutCirc",
        "easeInOutCirc" => "easeInOutCirc",
        "easeInOutBounce" => "easeInOutBounce",
        "easeInElastic" => "easeInElastic",
        "easeOutElastic" => "easeOutElastic",
        "easeInOutElastic" => "easeInOutElastic",
        "easeInBack" => "easeInBack",
        "easeOutBack" => "easeOutBack",
        "easeInOutBack" => "easeInOutBack",
        "easeInBounce" => "easeInBounce",
        "easeOutBounce" => "easeOutBounce"),
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

function single_page_settings_form_submit($form, &$form_state) {
  variable_set('single_page_load_mode', $form_state['values']['load_mode']);
  if (isset($form_state['values']['easing'])) {
    variable_set('single_page_easing', $form_state['values']['easing']);
  }
  drupal_set_message(t('The Single Page Module Settings has been saved.'));
}
