<?php
function single_page_page() {
  global $base_url;
  $module_path = drupal_get_path('module', 'single_page');
  // @todo: replace this with library call?
  if (!function_exists('str_get_html')) {
    include_once drupal_get_path('module', 'single_page') . '/includes/simple_html_dom/simple_html_dom.php';
  }

  drupal_add_js("$module_path/js/single_page.js");
  
  $path = libraries_get_path('jquery.easing') . '/jquery.easing.js';
  if (file_exists($path) && !module_exists('colorbox')) {
    drupal_add_js($path);
    $easing = variable_get('single_page_easing', 'jswing');
  }
  else {
    $easing = 'none';
    drupal_add_js(array('single_page' => array('easing' => 'none')), 'setting');
  }
  drupal_add_js(array('single_page' => array(
    'loader' => url('sp_loader'),
    'header_element' => variable_get('single_page_header', '#header'),
    'easing' => $easing,
    'footer_element' => variable_get('single_page_footer', '#footer-wrapper'), 'content_element' => variable_get('single_page_content', '#content .content'),
    'menu_element' => variable_get('single_page_menu', '#main-menu'))), 'setting');
  $messages = drupal_get_messages();

  $menu_name = variable_get('single_page_menu', '#main-menu');
  $menu_name = drupal_substr($menu_name, 1);
  $tree = menu_tree($menu_name);

  $tree_children = element_children($tree);
  
  $item = $tree[$tree_children[0]];
  // Get single page title from the first menu item
  // Is there a better way?
  $item['set_title'] = TRUE;

  // Prevent double loading.
  $loaded = array();
  $rendered = _single_page_load($item);
  $loaded[$item['#href']] = TRUE;

  // Render the first tree item for JS-disabled browsers
  $load_mode = variable_get('single_page_load_mode', 'all');
  
  $output = '<div id="single_page_wrapper">';
  if ($load_mode == 'one') {
    $output .= '<noscript>';
    $output .= $rendered; 
    $output .= '</noscript>';
  }
  else {
    $output .= $rendered;
  }
  if ($load_mode != 'one') {
    foreach (element_children($tree) as $key) {
      $item = $tree[$key];
      
      // No need to load already loaded links.
      if (array_key_exists($item['#href'], $loaded)) {
        continue;
      }
      $loaded[$item['#href']] = TRUE;
      $output .= _single_page_load($item);
    }
  }
  $output .= '</div>';
  
  return $output;
}

function single_page_load_ajax() {
  // @todo: replace this with library call?
  if (!function_exists('str_get_html')) {
    include_once drupal_get_path('module', 'single_page') . '/includes/simple_html_dom/simple_html_dom.php';
  }
  
  $url_to_load = $_GET['url'];
  $result = drupal_http_request(url($url_to_load, array(
    'absolute' => TRUE,
    'query' => array('single_page_load' => 1))));
  
  $html = str_get_html($result->data);
  $output = 'Not found';
  foreach ($html->find('#single_page_content_container') as $e) {
    $output = $e->innertext;
  }
  return array('status' => 1, 'html' => $output);
}

/**
 * Helper function to load menu item
 * @param array $item
 */
function _single_page_load($item) {
  // By default, show all pages
  $defaults = array(
    'hide' => FALSE,
    'set_title' => FALSE,
  );
  $item += $defaults;
  $output = '';
  if (isset($item['#title'])) {
    $href_original = $item['#href'];
    // Set the q to ... required to get the blok system working
    $_GET['single_page_load'] = 1;
    $q_orig = $_GET['q'];
    $_GET['q'] = $href_original;
    
    $href = url($href_original);
    // Strip starting slash.
    $href = substr($href, 1);
    $anchor = preg_replace('/[\/]/', '-', $href);

    if ($item['hide']) {
      $display = ' style="display:none"';
    }
    else {
      $display = '';
    }
    $output .= "<div id=\"$anchor\"></div>";
    $output .= '<div' . $display . ' id="' . $anchor . '_content" data-href="' . $href . '" class="single_page_wrapper"><div class="single_page">';

    $page_callback_result = menu_execute_active_handler($href_original, FALSE);

    // When the menu handled didn't return a node page (e.g. /contact), use the menu title
    if ($item['set_title']) {
      drupal_set_title($item['#title']);
    }

    // We mimic menu_execute_active_handler()
    $router_item = menu_get_item($href_original);
    if ($router_item['access']) {
      if ($router_item['include_file']) {
        require_once DRUPAL_ROOT . '/' . $router_item['include_file'];
      }
      $page_callback_result = call_user_func_array($router_item['page_callback'], $router_item['page_arguments']);
    }
    else {
      $page_callback_result = MENU_ACCESS_DENIED;
    }
    if (is_array($page_callback_result)) {
      $content = drupal_render_page($page_callback_result);
      $html = str_get_html($content);
      foreach ($html->find('#single_page_content_container') as $e) {
        $output .= $e->innertext;
      }
    }
    // @see for instance function views_page() which returns a string
    elseif (is_string($page_callback_result)) {
      $output .= $page_callback_result;
    }
    else {
      // Report error
      $output .= "<p>An error '" . $page_callback_result . "' occurred for " . $item['#href'] . '</p>';
    }
    $_GET['q'] = $q_orig;
    unset($_GET['single_page_load']);
    $output .= '</div></div>';

    return $output;
    
    
    
    $result = drupal_http_request(url($href, array(
      'absolute' => TRUE,
      'query' => array('single_page_load' => 1))));
    
    $html = str_get_html($result->data);
    
    foreach ($html->find('#single_page_content_container') as $e) {
      $output .= $e->innertext;
    }
  
    $output .= '</div></div>';
  }

  return $output;
}