(function($){
	// Taken from http://lions-mark.com/jquery/scrollTo/
	$.fn.scrollTo = function( target, options, callback ){
		  if(typeof options == 'function' && arguments.length == 2){ callback = options; options = target; }
		  var settings = $.extend({
		    scrollTarget  : target,
		    offsetTop     : 50,
		    duration      : 500,
		    easing        : 'swing'
		  }, options);
		  return this.each(function(){
		    var scrollPane = $(this);
		    var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
		    var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
		    scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
		      if (typeof callback == 'function') { callback.call(this); }
		    });
		  });
  };

  $(document).ready(function(){
	Drupal.settings.single_page.current='';
	//$('#single_page_wrapper').after('<div id="single_page_placeholder" style="position:absolute"></div>');
    //if (!Drupal.settings.single_page.easing) Drupal.settings.single_page.easing='swing';
	Drupal.singlepage = function(original_url) {
	  new_url = original_url.replace(/[\/]/g, '-');
	  if (window.location.pathname == Drupal.settings.single_page.base_url && new_url == Drupal.settings.single_page.current) return;
	  Drupal.settings.single_page.current=new_url;
	  window.location.href = Drupal.settings.single_page.base_url + '#' + new_url;
	  //window.location.hash = '#' + new_url;
	  // Tmp!
	  prev = $(window.location.hash).prev('.single_page_wrapper');
	  if (!prev.length) {
		window.scrollTo(0, 0);
	  }
	  // End tmp
	  return;
	  window.scrollTo(0,0);
	  if ($('#'+new_url).length) {
		newhtml = $($('#'+new_url).html());
		$("#single_page_placeholder").height($("#single_page_wrapper").height()).css('position', 'relative');
		$("#single_page_wrapper").hide(function() {
		  $(this).html(newhtml);
		  $("#single_page_wrapper").show(Drupal.settings.single_page.easing,function(){
		   $("#single_page_placeholder").css('position', 'absolute');
		  });
		});
		//fix duplicate content
		$('#'+new_url).hide();
	  }
	  else {
       $.get(Drupal.settings.single_page.loader + '?url=' + original_url,
        function(data, textStatus, jqXHR) {
    	  if (data.status==1) {
    	   var newdiv=$('<div id="'+window.location.hash.substring(1)+'">'+data.html+'</div>').hide();
    	   $('body').append(newdiv);
   		   $("#single_page_placeholder").height($("#single_page_wrapper").height()).css('position', 'relative');
		   $("#single_page_wrapper").hide(function() {
		    $(this).html(data.html);
		    $("#single_page_wrapper").show(Drupal.settings.single_page.easing,function(){
		     $("#single_page_placeholder").css('position', 'absolute');
		    });
		   });
    	  }
        }
       );
	  }
	};
    var menu_element = Drupal.settings.single_page.menu_element;
    var basePath = Drupal.settings.basePath;
    var pathPrefix = Drupal.settings.pathPrefix;
    var from_hash=false;
    $(menu_element+" a").each(function(index) {
      $(this).data('href_orig', this.pathname.substring(1));
      if (window.location.hash=='#'+this.pathname.substring(1)) {
    	from_hash=true;
    	Drupal.singlepage(this.pathname.substring(1));
      }
    });
    // No page specified in URL, load the first menu item
    if (Drupal.settings.single_page.loader && !from_hash){
     Drupal.singlepage($(menu_element+" a:first").get(0).pathname.substring(1));
    }
	  
	$(".single_page_wrapper").css("clear", "both");
    $(".single_page_wrapper .single_page").css("overflow", "inherit");
    var header_elemint = Drupal.settings.single_page.header_element;
    var footer_element = Drupal.settings.single_page.footer_element;
    var menu_element = Drupal.settings.single_page.menu_element;
    var content_element = Drupal.settings.single_page.content_element;
    var easing = Drupal.settings.single_page.easing;
    $(menu_element + " li:first").addClass("active");
    $(menu_element + " li:first a").addClass("active");
    var anchor = location.hash;
    $(menu_element+" a").click(function(event){
      $(menu_element + " li").removeClass("active");
      $(menu_element + " li a").removeClass("active");
      $(this).addClass("active");
      $(this).parent().addClass("active");
      $(this).addClass("active");
      //prevent the default action for the click event
      event.preventDefault();

      //get the full url - like mysitecom/index.htm#home
      var original_url = encodeURIComponent($(this).data('href_orig'));
      Drupal.singlepage(original_url);
      // @todo: bring back easing effects
    });
  });
})(jQuery);